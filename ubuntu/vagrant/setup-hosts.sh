#!/bin/bash
set -e
IFNAME=$1
ADDRESS="$(ip -4 addr show $IFNAME | grep "inet" | head -1 |awk '{print $2}' | cut -d/ -f1)"
sed -e "s/^.*${HOSTNAME}.*/${ADDRESS} ${HOSTNAME} ${HOSTNAME}.local/" -i /etc/hosts

# remove ubuntu-bionic entry
sed -e '/^.*ubuntu-bionic.*/d' -i /etc/hosts

# Update /etc/hosts about other hosts
cat >> /etc/hosts <<EOF
192.168.100.101  k3s-master.local
192.168.100.102  k3s-worker-1.local
192.168.100.103  k3s-worker-2.local
192.168.1.101  k3s-master.public
192.168.1.102  k3s-worker-1.public
192.168.1.103  k3s-worker-2.public

EOF
