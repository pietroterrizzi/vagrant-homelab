#!/bin/bash
set -e

#STATIC VARS
NAT_NET_GW="10.0.2.2"
PUB_NET_GW="192.168.1.1"
K3S_SERVICE="k3s"

#MAIN
#CHECKING NAT DEFAULT GW
if netstat -nr | awk '{ print $2 }' | grep "$NAT_NET_GW"; then
        echo "$NAT_NET_GW exists - REMOVING"
	sudo route delete default gw $NAT_NET_GW
else
        echo "$NAT_NET_GW does not exist - SKIPPING"
fi

#CHECKING PUBLIC DEFAULT GW
if netstat -nr | awk '{ print $2 }' | grep "$PUB_NET_GW"; then
        echo "$PUB_NET_GW already in routing tables - SKIPPING"
else
        echo "$PUB_NET_GW does not exist - ADDING"
	sudo route add default gw $PUB_NET_GW
fi

#CHECKING K3S SERVICE
if systemctl list-units --full -all | grep "$K3S_SERVICE"; then
	echo "$K3S_SERVICE exists - RESTARTING SERVICE"
        sudo systemctl restart k3s*
else
	echo "$K3S_SERVICE does not exist - SKIPPING"
fi

