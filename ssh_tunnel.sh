#!/bin/bash

#set -e
#set -o nounset
set -o pipefail

#PARAMS
func=$1
extra_arg1=$2

#FUNC
if [[ $func = "workdir_init" ]]; then
workdir_init(){

ssh -tt "$CED_USER"@"$CED_IP" << TERRIZZI

mkdir -p "$IAAS_FOLDER"

exit

TERRIZZI
} && workdir_init
fi

if [[ $func = "status" ]]; then
status(){

ssh -tt "$CED_USER"@"$CED_IP" << TERRIZZI

pushd "$IAAS_FOLDER"

vagrant status

popd

exit

TERRIZZI
} && status
fi

if [[ $func = "up" ]]; then
up(){

ssh -tt "$CED_USER"@"$CED_IP" << TERRIZZI

pushd "$IAAS_FOLDER"

vagrant up

popd

exit

TERRIZZI
} && up
fi

if [[ $func = "provision" ]]; then
provision(){

ssh -tt "$CED_USER"@"$CED_IP" << TERRIZZI

pushd "$IAAS_FOLDER"

vagrant provision

popd

exit

TERRIZZI
} && provision
fi

if [[ $func = "destroy" ]]; then
destroy(){

ssh -tt "$CED_USER"@"$CED_IP" << TERRIZZI

pushd "$IAAS_FOLDER"

vagrant destroy -f

popd

exit

TERRIZZI
} && destroy
fi
